import { State } from "./types";
import { GetterTree, MutationTree } from "vuex";

export const state: State = {
  elements: []
};

export const getters: GetterTree<State, any> = {
  elements: state =>
    state.elements.map(e => {
      const { id, w, h, x, y, z } = e;
      const style = {
        color: e.color,
        backgroundColor: e.backgroundColor
      };
      return { id, w, h, x, y, z, style };
    })
};

export const mutations: MutationTree<State> = {
  emptyState() {
    this.replaceState({ elements: [] });
  },
  addElement(state, newElement) {
    state.elements.push({ ...newElement });
  },
  removeElement(state, elementId) {
    let elementIndex = state.elements.findIndex(e => e.id === elementId);
    if (elementIndex > -1) {
      state.elements.splice(elementIndex, 1);
    }
  },
  positionElement(state, payload) {
    let e = state.elements.find(e => e.id === payload.id);

    if (typeof e === "undefined") {
      return;
    }

    e.y = payload.y;
    e.x = payload.x;
    e.w = payload.w;
    e.h = payload.h;
    e.z = payload.z;
  }
};
