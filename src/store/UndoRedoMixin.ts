import { Vue, Component } from "vue-property-decorator";
import { MutationPayload } from "vuex";

const EMPTY_STATE = "emptyState";

@Component({})
export default class UndoRedo extends Vue {
  done: MutationPayload[] = [];
  undone: MutationPayload[] = [];
  newMutation = true;

  created() {
    this.$store.subscribe((mutation: MutationPayload) => {
      if (mutation.type !== EMPTY_STATE) {
        this.done.push(mutation);
      }
      if (this.newMutation) {
        this.undone = [];
      }
    });
  }

  undo() {
    this.undone.push(this.done.pop() as MutationPayload);
    this.newMutation = false;
    this.$store.commit(EMPTY_STATE);
    this.done.forEach(mutation => {
      this.$store.commit(`${mutation.type}`, mutation.payload);
      this.done.pop();
    });
    this.newMutation = true;
  }

  redo() {
    let commit = this.undone.pop() as MutationPayload;
    this.newMutation = false;
    this.$store.commit(`${commit.type}`, commit.payload);
    this.newMutation = true;
  }

  get canUndo() {
    return this.done.length;
  }

  get canRedo() {
    return this.undone.length;
  }
}