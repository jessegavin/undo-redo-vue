export interface Position {
  y: number;
  x: number;
  w: number;
  h: number;
  z: number;
}

export interface Colors {
  color: string;
  backgroundColor: string;
}

export interface Element extends Position, Colors {
  id: string;
}

export interface State {
  elements: Element[];
}
