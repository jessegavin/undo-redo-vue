<template>
  <div id="app">
    <div class="canvas-wrap" ref="canvas">
      <Canvas />
    </div>
    <div class="toolbar">
      <div>
        <button @click="undo" :disabled="!canUndo">Undo</button>
        <button @click="redo" :disabled="!canRedo">Redo</button>
      </div>
      <CreateElementButton
        :height="dimensions.height"
        :width="dimensions.width"
      />

      <div class="history">
        <div :key="$index" v-for="(m, $index) in history">
          {{ m.type }} : {{ m.id }}
        </div>
      </div>
    </div>
  </div>
</template>

<script lang="ts">
import { Component, Vue, Mixins } from "vue-property-decorator";
import { Mutation } from "vuex-class";
import { MutationMethod } from "vuex";
import UndoRedo from "./store/UndoRedoMixin";
import Canvas from "./components/Canvas.vue";
import CreateElementButton from "./components/CreateElementButton.vue";

@Component({
  components: { Canvas, CreateElementButton }
})
export default class App extends Mixins(UndoRedo) {
  @Mutation addElement: MutationMethod;
  dimensions: any = {};

  created() {
    window.addEventListener("keydown", this.onKey);
  }

  mounted() {
    let canvas = this.$refs.canvas as HTMLElement;
    this.dimensions = canvas.getBoundingClientRect();
  }

  beforeDestroy() {
    window.removeEventListener("keydown", this.onKey);
  }

  onKey(e: any) {
    if (e.keyCode == 90 && e.ctrlKey && this.canUndo) {
      this.undo();
      return;
    }
    if (e.keyCode == 89 && e.ctrlKey && this.canRedo) {
      this.redo();
      return;
    }
  }

  get history() {
    return this.done.map(d => {
      if (d.type === "addElement") {
        return { type: "add", id: d.payload.id };
      }
      if (d.type === "removeElement") {
        return { type: "remove", id: d.payload };
      }
      if (d.type === "positionElement") {
        return { type: "position", id: d.payload };
      }
    });
  }
}
</script>

<style>
* {
  box-sizing: border-box;
}

html,
body {
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;
  font-family: Helvetica, Arial, sans-serif;
}

#app {
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  display: flex;
  width: 100%;
  height: 100%;
  align-items: stretch;
  overflow: hidden;
}

.canvas-wrap {
  width: 75%;
  position: relative;
}
.canvas {
  position: relative;
  overflow: hidden;
  width: 100%;
  height: 100%;
}
.toolbar {
  width: 25%;
  background-color: gray;
  display: flex;
  flex-direction: column;
  align-items: center;
}

.element {
  position: absolute;
  opacity: 0.8;
  display: flex;
  justify-content: center;
  align-items: center;
  border: solid 1px black;
}

.history {
  flex: 1;
  overflow: hidden;
}

button {
  font-size: 2rem;
  margin: 1rem;
}
</style>
